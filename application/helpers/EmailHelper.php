<?php
namespace app\helpers;

use yii;

/**
 */
class EmailHelper{

	public $template; //тело письма (string)
	public $emailTo; //email на которой отправляем письмо (string)
	public $data; //массив данных для отправки (massive)

	public function send(){
		yii::$app->mailer->compose($this->template, $this->data)
         ->setFrom(yii::$app->params['fromEmail'])
         ->setTo($this->emailTo)
         ->setSubject($this->subject)
         ->send();
	}
}
