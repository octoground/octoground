<?php 
	use yii\helpers\Url;
?>
<div class="og-container">
	<div class="advantages">
		<img src="<?= Url::to(['images/default/icon/dollar.png']) ?>" alt="Приятные цены">
		<div class="body">
			<h6>Приятные цены</h6>
			<!-- <div class="line-horizont"></div> -->
			<p>В первую очередь мы думаем о наших клиентах и поэтому выстраиваем демократичные цены на услуги.</p>
		</div>
	</div>
	<div class="advantages">
		<img src="<?= Url::to(['images/default/icon/innovation.png']) ?>" alt="Инновационные решения">
		<div class="body">
			<h6>Современный подход</h6>
			<!-- <div class="line-horizont"></div> -->
			<p>В разработке проектов мы исопльзуем исключительно соврменные технологии и методы. Таким образом мы обеспечиваем надежность, качество и быстродействие.</p>
		</div>
	</div>
	<div class="advantages">
		<img src="<?= Url::to(['images/default/icon/raceta.png']) ?>" alt="Быстрый запуск">
		<div class="body">
			<h6>Быстрый запуск</h6>
			<!-- <div class="line-horizont"></div> -->
			<p>Мы следуем вырожению: “Время - деньги”. Мы работаем поэтапно, что позволяет выводить проект в “свет” в кратчайшие сроки.</p>
		</div>
	</div>
</div>