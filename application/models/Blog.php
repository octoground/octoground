<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property string $subject
 * @property string $author
 * @property string $post
 * @property string $main_img
 * @property string $read_img
 * @property string $text
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['text'], 'string'],
            [['subject', 'author', 'post'], 'string', 'max' => 50],
            [['main_img', 'read_img', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'author' => 'Author',
            'post' => 'Post',
            'main_img' => 'Main Img',
            'read_img' => 'Read Img',
            'text' => 'Text',
            'description' => 'description',
        ];
    }
}
