<?php
/** @var \app\models\Field[] $fields */
/** @var \app\modules\edit\models\DynamicModel $model */
/** @var \app\models\ImageCropperSettings $settingsCropper */
use yii\helpers\Url;
use vova07\imperavi\Widget;
?>
<div class="row">
    <div class="col-lg-12">
        <?php $form = \yii\widgets\ActiveForm::begin(); ?>
        <?php foreach ($fields as $field): ?>
            <div class="form-group">
                <?php switch ($field->type->name) {
                    default:
                    case 'textInput':
                        echo $form->field($model, $field->name)->textInput(['class' => 'form-control'])->label($field->alias);
                        break;
                    case 'textArea':
//                        echo $form->field($model, $field->name)->textarea(['class' => 'form-control tiny-editor'])->label($field->alias);
                        echo $form->field($model, $field->name)->widget(dosamigos\ckeditor\CKEditor::className(), [
                            'kcfinder' => true,
                            'preset' => 'full',
                            'kcfOptions' => [
                                'uploadURL' => '@web/images/upload',
                                'uploadDir' => '@webroot/../images/upload',
                                'access' => [  // @link http://kcfinder.sunhater.com/install#_access
                                    'files' => [
                                        'upload' => true,
                                        'delete' => true,
                                        'copy' => true,
                                        'move' => true,
                                        'rename' => true,
                                    ],
                                    'dirs' => [
                                        'create' => true,
                                        'delete' => true,
                                        'rename' => true,
                                    ],
                                ],
                                'types' => [  // @link http://kcfinder.sunhater.com/install#_types
                                    'files' => [
                                        'type' => '',
                                    ],
                                ],
                            ],
                        ]);
                        break;
                    case 'textAreaNoEditor':
                        echo $form->field($model, $field->name)->textarea(['class' => 'form-control', 'rows' => 10])->label($field->alias);
                        break;
                    case 'uploadImage':
                        echo $this->render('templates/upload_image', [
                            'field' => $field,
                            'model' => $model,
                            'form' => $form,
                        ]);
                        break;
                    case 'imageCropper':
                        echo $this->render('templates/imageCropper', [
                            'field' => $field,
                            'model' => $model,
                            'form' => $form,
                            'settingsCropper' => $settingsCropper[$field->name],
                        ]);
                        break;
                    case 'datetime':
                        echo $form->field($model, $field->name)->textInput(['class' => 'form-control datetimepicker'])->label($field->alias);
                        break;
                    case 'date':
                        echo $form->field($model, $field->name)->textInput(['class' => 'form-control  datepicker'])->label($field->alias);
                        break;
                    case 'time':
                        echo $form->field($model, $field->name)->textInput(['class' => 'form-control  timepicker'])->label($field->alias);
                        break;
                    case 'boolean':
                        echo $form->field($model, $field->name)->dropDownList([0 => 'Нет', 1 => 'Да'], ['class' => 'form-control'])->label($field->alias);
                        break;
                    case 'dropDownList':
                        echo $form->field($model, $field->name)->dropDownList($model->getRelationArray($field->name), ['class' => 'form-control'])->label($field->alias);
                        break;
                } ?>
            </div>
        <?php endforeach; ?>
        <?php if($orders): ?>

                <style>
                    .orderInfo span{
                        font-weight: bold;
                    }
                    .list{
                        display: grid;
                        grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
                    }
                    .list img{
                        width: 300px;
                        height: 300px;
                        object-fit: contain;
                    }
                    .product{
                        display: flex;
                        flex-wrap: wrap;
                        justify-content: center;
                        border: 2px solid #00908b;
                        width: 95%;
                        margin-bottom: 15px;
                    }
                    .product-info{
                        width: 100%;
                        padding: 10px 20px;
                    }
                    .product-info .productName{
                        text-align: center;
                        font-size: 23px;
                        font-weight: bold;
                    }
                    .product-info section, .row{
                        font-size: 17px;
                    }
                    .product-info section span{
                        font-weight: bold;
                    }
                </style>
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Товары</h3>
                        <div class="list">

                            <?php foreach ($orders as $product): ?>
                                <div class="product">
                                    <img src="<?= Url::to(['../../'.$product->img]) ?>" alt="asdasd">
                                    <div class="product-info">

                                        <div class="productName"><?= $product->title ?></div>
                                        <section>
                                            <?php if($product->color_product): ?>
                                                <div><span>Цвет: </span><?= $product->color_product ?></div>
                                            <?php endif;?>
                                            <?php if($product->material_product): ?>
                                                <div><span>Материал: </span><?= $product->material_product ?></div>
                                            <?php endif;?>
                                            <?php if($product->side_product): ?>
                                                <div><span>Стенки: </span><?= $product->side_product ?></div>
                                            <?php endif;?>
                                            <?php if($product->size_product): ?>
                                                <div><span>Размер: </span><?= $product->size_product ?></div>
                                            <?php endif;?>

                                            <div><span>Количество: </span><?= $product->count ?></div>
                                            <div><span>Стоимость: </span><?= $product->cost ?></div>
                                        </section>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>

        <?php endif;?>
        <button type="submit" class="btn btn-default">Сохранить</button>
        <?php $form->end(); ?>
    </div>
</div>